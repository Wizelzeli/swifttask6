// Реализовать структуру IOSCollection и создать в ней write по типу сорy on

struct IOSCollection {
	var num = 42
}

class Ref<T> {
	var value: T

	init (value: T) {
		self.value = value
	}
} 

struct Conteuner<T> {
	var ref: Ref<T>

	init (value: T) {
		self.ref = Ref(value: value)
	} 

	var value: T {

		get { ref.value }
			 
		set {
			guard(isKnownUniquelyReferenced(&ref)) else {
				ref = Ref(value: newValue) 
				return
			}
			ref.value = newValue
		}
	}
}


// Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол

protocol Hotel {
	init (roomCount: Int)
}
class HotelAlfa: Hotel {
	var roomCount: Int
	required init (roomCount: Int) { 
		self.roomCount = roomCount
	}
}

// Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'

protocol GameDice {
	var numberDice: String {
		get
	}
}

extension Int: GameDice {
	var numberDice: String {
	return "Выпало \(self) на кубике"
	}
}
let diceCoub = 4 
print(diceCoub.numberDice)

// Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство

@objc protocol SomeProtocol {
	var name: String {get set}
	@objc optional var secondName: String {get set}
	func someFunc() -> String
}

class SomeClass: SomeProtocol {
	func someFunc() -> String {
		return "Some String"
	}
	
	var name: String

	init(name: String){
		self.name = name
	}
}

// Изучить раздел 'Протоколы -> Делегирование' в документации
// Проработать код из видео
// Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). 
// Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)

protocol WriteCode {
	var time: Int {get set}
	var codeLines: Int {get set}
	func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol StopCode {
	func stopCoding()
}

enum Platform: String {
	case Ios = "IOS"
	case Android = "Android"
	case Web = "Web"
}

class Company: WriteCode, StopCode {
	var numberOfSpecialist: Int
	var platform: Platform
	var time: Int
	var codeLines: Int

	init (numberOfSpecialist: Int, platform: Platform, time: Int, codeLines: Int) {
		self.numberOfSpecialist = numberOfSpecialist
		self.platform = platform
		self.time = time
		self.codeLines = codeLines
	}

	func writeCode(platform: Platform, numberOfSpecialist: Int) {
		print("Разработка началась. Пишем код под \(platform.rawValue)")
	}

	func stopCoding() {
		print("Работа закончена. Сдаю в тестирование.")
	}

}